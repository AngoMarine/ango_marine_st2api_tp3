/* api Call : pour avoir la meteo du jour*/
let apiCall = function(city) {
    let url = "https://api.openweathermap.org/data/2.5/weather?q=" + city + "&units=metric&appid=53471f3e4cd6be26d6efc764c128bb3c";
    console.log(url)
    fetch(url).then((response) =>
        response.json().then((data) => {
            console.log(data);
            document.querySelector('#city').innerHTML = data.name;
            document.querySelector('#temp').innerHTML = "<i class='fa-solid fa-temperature-arrow-up'></i>" + data.main.temp + '°';
            document.querySelector('#humidity').innerHTML = "<i class='fa-solid fa-droplet'></i>" + data.main.humidity + '%';
            document.querySelector('#wind').innerHTML = "<i class='fa-solid fa-wind'></i>" + data.wind.speed + 'km/h';
            document.querySelector('#latitude').innerHTML = "<i class='fa-solid fa-globe'></i> " + data.coord.lat + '°';
            document.querySelector('#longitude').innerHTML = "<i class='fa-solid fa-globe'></i> " + data.coord.lon + '°';
        })
    ).catch((err) =>
        console.log('Erreur :' + err)
    );

}

/* api Call : pour avoir la meteo des prochains jours pour une ville données*/
let apiCall5Days = function(city) {

    let url = "https://api.openweathermap.org/data/2.5/forecast?q=" + city + "&appid=53471f3e4cd6be26d6efc764c128bb3c&units=metric";
    console.log(url)
    fetch(url).then((response) =>
        response.json().then((data) => {
            console.log(data);
            document.querySelector('#day1Min').innerHTML = "<i class='fa-solid fa-temperature-empty'></i> " + data.list[0].main.temp_min;
            document.querySelector('#day1Max').innerHTML = "<i class='fa-solid fa-temperature-full'></i> " + data.list[0].main.temp_max;
            document.querySelector('#day2Min').innerHTML = "<i class='fa-solid fa-temperature-empty'></i> " + data.list[1].main.temp_min;
            document.querySelector('#day2Max').innerHTML = "<i class='fa-solid fa-temperature-full'></i> " + data.list[1].main.temp_max;
            document.querySelector('#day3Min').innerHTML = "<i class='fa-solid fa-temperature-empty'></i> " + data.list[2].main.temp_min;
            document.querySelector('#day3Max').innerHTML = "<i class='fa-solid fa-temperature-full'></i> " + data.list[2].main.temp_max;
            document.querySelector('#day4Min').innerHTML = "<i class='fa-solid fa-temperature-empty'></i> " + data.list[3].main.temp_min;
            document.querySelector('#day4Max').innerHTML = "<i class='fa-solid fa-temperature-full'></i> " + data.list[3].main.temp_max;
            document.querySelector('#day5Min').innerHTML = "<i class='fa-solid fa-temperature-empty'></i> " + data.list[4].main.temp_min;
            document.querySelector('#day5Max').innerHTML = "<i class='fa-solid fa-temperature-full'></i> " + data.list[4].main.temp_max;

            document.querySelector('#humidityDay1').innerHTML = "<i class='fa-solid fa-droplet'></i> " + data.list[0].main.humidity + ' %';
            document.querySelector('#humidityDay2').innerHTML = "<i class='fa-solid fa-droplet'></i> " + data.list[1].main.humidity + ' %';
            document.querySelector('#humidityDay3').innerHTML = "<i class='fa-solid fa-droplet'></i> " + data.list[2].main.humidity + ' %';
            document.querySelector('#humidityDay4').innerHTML = "<i class='fa-solid fa-droplet'></i> " + data.list[3].main.humidity + ' %';
            document.querySelector('#humidityDay5').innerHTML = "<i class='fa-solid fa-droplet'></i> " + data.list[4].main.humidity + ' %';

            document.querySelector('#windDay1').innerHTML = "<i class='fa-solid fa-wind'></i>" + data.list[0].wind.speed + 'km/h';
            document.querySelector('#windDay2').innerHTML = "<i class='fa-solid fa-wind'></i>" + data.list[1].wind.speed + 'km/h';
            document.querySelector('#windDay3').innerHTML = "<i class='fa-solid fa-wind'></i>" + data.list[2].wind.speed + 'km/h';
            document.querySelector('#windDay4').innerHTML = "<i class='fa-solid fa-wind'></i>" + data.list[3].wind.speed + 'km/h';
            document.querySelector('#windDay5').innerHTML = "<i class='fa-solid fa-wind'></i>" + data.list[4].wind.speed + 'km/h';

        })

    ).catch((err) => console.log('Erreur :' + err));

}

/* Ecouteur d'evenement */

document.querySelector('form').addEventListener('submit', function(e) {
    e.preventDefault();
    let city = document.querySelector('#inputCity').value;
    console.log(city);
    apiCall(city);
    apiCall5Days(city);
})


//Getting and displaying the text for the upcoming five days of the week
var d = new Date();
var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", ];

//Function to get the correct integer for the index of the days array
function CheckDay(day) {
    if (day + d.getDay() > 6) {
        return day + d.getDay() - 7;
    } else {
        return day + d.getDay();
    }
}

for (i = 0; i < 5; i++) {
    document.getElementById("day" + (i + 1)).innerHTML = weekday[CheckDay(i)];
}