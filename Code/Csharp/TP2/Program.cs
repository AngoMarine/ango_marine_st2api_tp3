﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using TP2;

static async Task<Root> getWeather(string customRequest)
{
    var client = new HttpClient();
    var request = new HttpRequestMessage
    {
        Method = HttpMethod.Get,
        RequestUri =
            new Uri(String.Format("https://api.openweathermap.org/data/2.5/weather?{0}&units=metric&appid=53471f3e4cd6be26d6efc764c128bb3c", customRequest)) // my key is: 53471f3e4cd6be26d6efc764c128bb3c
    };
    using (var response = await client.SendAsync(request))
    {
        response.EnsureSuccessStatusCode();
        var body = await response.Content.ReadAsStringAsync();
        Root myDeserializedClass = JsonSerializer.Deserialize<Root>(body);
        return myDeserializedClass;
    }
}

static async Task<string> Weater(string city)
{
    Console.WriteLine("What’s the weather like in "+ city +"?");
    try
    {
        Root resultWeather = await getWeather("q="+ city);
        Console.WriteLine("The weather in "+ city +" is : " + resultWeather.weather[0].main);
        return resultWeather.weather[0].main;
    }
    catch (Exception e)
    {
        Console.Write("An error is produced in the API call");
        Console.WriteLine("{0} Exception caught.", e);
        return e.Message;
    }
    
}

static async void SunRiseSet(string city)
{
    Console.WriteLine("When will the sun rise and set today in "+ city +" ?");
    try
    {
        Root sunTime = await getWeather("q="+city);
        DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        dateTime = dateTime.AddSeconds(sunTime.sys.sunrise).ToLocalTime();
        Console.WriteLine("The sun in "+ city +" rise at : " + dateTime);

        DateTime dateRes = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        dateRes = dateRes.AddSeconds(sunTime.sys.sunset).ToLocalTime();
        Console.WriteLine("The sun in "+ city +" set at : " + dateRes);
        Console.WriteLine();
    }
    catch (Exception e)
    {
        Console.Write("An error is produced in the API call");
        Console.WriteLine("{0} Exception caught.", e);
        //return e;
    }

}

static async Task<string> SunRise(string city)
{
    Console.WriteLine("When will the sun rise and set today in " + city + " ?");
    try
    {
        Root sunTime = await getWeather("q=" + city);
        DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        dateTime = dateTime.AddSeconds(sunTime.sys.sunrise).ToLocalTime();
        Console.WriteLine("The sun in " + city + " rise at : " + dateTime);
        return dateTime.ToString();
    }
    catch (Exception e)
    {
        Console.Write("An error is produced in the API call");
        Console.WriteLine("{0} Exception caught.", e);
        return e.Message;
    }

}

static async Task<string> SunSet(string city)
{
    try
    {
        Root sunTime = await getWeather("q=" + city);
        
        DateTime dateRes = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        dateRes = dateRes.AddSeconds(sunTime.sys.sunset).ToLocalTime();
        Console.WriteLine("The sun in " + city + " set at : " + dateRes);
        return dateRes.ToString();
    }
    catch (Exception e)
    {
        Console.Write("An error is produced in the API call");
        Console.WriteLine("{0} Exception caught.", e);
        return e.Message;
    }

}

static async Task<string> temperature(string city)
{
    Console.WriteLine("What’s the temperature in "+city+" (in Celsius)?");
    try
    {
        Root temperature = await getWeather("q="+city);
        Console.WriteLine("The temperature in "+city+" is : " + temperature.main.temp + " degrees Celsius");
        return temperature.main.temp.ToString();
    }
    catch (Exception e)
    {
        Console.Write("An error is produced in the API call");
        Console.WriteLine("{0} Exception caught.", e);
        return e.Message;
    }
}

static async Task<string> windy(string city)
{
    try
    {
        Root windy = await getWeather("q="+city);
        Console.WriteLine("The wind in "+city +" is : " + windy.wind.speed + " km/h");
        return windy.wind.speed.ToString();
    }
    catch (Exception e)
    {
        Console.Write("An error is produced in the API call");
        Console.WriteLine("{0} Exception caught.", e);
        return e.Message;
    }
    
}

/*
Console.WriteLine("Where is it more windy? New-York, Tokyo or Paris?");
try
{
    Root NewYork = await getWeather("q=New%20York");
    Root Tokyo = await getWeather("q=Tokyo");
    Root Paris = await getWeather("q=Paris");

    if (NewYork.wind.speed >= Tokyo.wind.speed && NewYork.wind.speed >= Paris.wind.speed)
    {
        Console.WriteLine("The most windy city is New-York with a wind speed of : " + NewYork.wind.speed + " km/h");
    }
    else if (Tokyo.wind.speed >= NewYork.wind.speed && Tokyo.wind.speed >= Paris.wind.speed)
    {
        Console.WriteLine("The most windy city is Tokyo with a wind speed of : " + Tokyo.wind.speed + " km/h");
    }
    else
    {
        Console.WriteLine("The most windy city is Paris with a wind speed of : " + Paris.wind.speed + " km/h");
    }
}
catch (Exception e)
{
    Console.Write("An error is produced in the API call");
    Console.WriteLine("{0} Exception caught.", e);
}
Console.WriteLine();
*/

static async Task<string> humidity(string city)
{
    Console.WriteLine("What is the humidity in "+city+"?");
    try
    {
        Root humidity = await getWeather("q="+city);
        Console.WriteLine("The humidity in "+city+" is : " + humidity.main.humidity +" %");
        return humidity.main.humidity.ToString();

    }
    catch (Exception e)
    {
        Console.Write("An error is produced in the API call");
        Console.WriteLine("{0} Exception caught.", e);
        return e.Message;
    }

}

static async Task<string> pressure(string city)
{
    Console.WriteLine("What is the pressure in " + city + "?");
    try
    {
        Root pressure = await getWeather("q=" + city);
        Console.WriteLine("the pressure in "+city+ " is : " + pressure.main.pressure + " hPa");
        return pressure.main.pressure.ToString();

    }
    catch (Exception e)
    {
        Console.Write("An error is produced in the API call");
        Console.WriteLine("{0} Exception caught.", e);
        return e.Message;
    }

}

static async void DaylyInfo(string city)
{
    await Weater(city);
    await SunRise(city);
    await SunSet(city);
    await temperature(city);
    await windy(city);
    await humidity(city);
    await pressure(city);
}

DaylyInfo("Paris");

Console.ReadKey();

// to generate my classes, I use https://json2csharp.com/ and I convert Json file (see the example of an API response bellow) to csharp

/* EXAMPLE OF API RESPONSE
 *{
  "coord": {
    "lon": -122.08,
    "lat": 37.39
  },
  "weather": [
    {
      "id": 800,
      "main": "Clear",
      "description": "clear sky",
      "icon": "01d"
    }
  ],
  "base": "stations",
  "main": {
    "temp": 282.55,
    "feels_like": 281.86,
    "temp_min": 280.37,
    "temp_max": 284.26,
    "pressure": 1023,
    "humidity": 100
  },
  "visibility": 10000,
  "wind": {
    "speed": 1.5,
    "deg": 350
  },
  "clouds": {
    "all": 1
  },
  "dt": 1560350645,
  "sys": {
    "type": 1,
    "id": 5122,
    "message": 0.0139,
    "country": "US",
    "sunrise": 1560343627,
    "sunset": 1560396563
  },
  "timezone": -25200,
  "id": 420006353,
  "name": "Mountain View",
  "cod": 200
  }
 */