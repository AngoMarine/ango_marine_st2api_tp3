# ANGO_Marine_ST2API_TP3

## Getting started

Hello Julien !
Tu pourra voir des écrans de l'interface dans le dossier qui se trouve : ango_marine_st2api_tp3
Il y a 4 exemples : Morocco, paris, Moscow, new york

Je n'ai pas pu réaliser d'interface WPF car j'ai un mac, je n'ai pas pu utiliser Xamarin car cela est lié a Xcode quand on a un mac donc j'ai fais autrement. 

Pour lancer le Csharp : normalement double clic sur la solution sln

## Add your files

```
cd existing_repo
git remote add origin https://gitlab.com/AngoMarine/ango_marine_st2api_tp3.git
git branch -M main
git push -uf origin main
```

## Name
Meteo Project

## Description
Ce projet permet d'avoir des informations sur la météo. En rentrant une ville dans la barre de recherche on peut accéder aux données de l'API open Weather. 

